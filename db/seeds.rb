MOVIES = [
  { title: "The Train Job", description: "Mal and his crew pull a train robbery."},
  { title: "Bushwhacked", description: "The crew of Serenity stumble upon the ruins of a transport ship and decide to search the vessel for any salvageable goods, only to discover that the ship is not entirely deserted."},
  { title: "Our Mrs. Reynolds", description: "A woman claiming to be Mal's wife appears aboard Serenity."},
  { title: "Jaynestown", description: "Mal and the crew of Serenity land to pick up some contraband In Canton, a township on Higgins' Moon, where labor is provided under servitude run by the local magistrate. Jayne had once robbed the boss with a partner but barely escaped by jettisoning from their getaway vehicle his partner (now waiting for revenge) and the money boxes (who have made him a legendary hero). Jayne is recognized by all and \"welcomed\" accordingly. Sub-plots show Simon and Kaylee face their growing romantic feelings, River first tries to \"fix\" the Bible and is later traumatized by seeing ..."},
  { title: "Out of Gas", description: "When Serenity's life support system fails, Mal orders the crew off the ship - but he stays behind."},
  { title: "Shindig", description: "After scuffling with one of Inara's clients during a royal ball where the Serenity crew is hired by a diplomat to smuggle some unusual cargo, Mal is challenged to a duel of swords by the snobbish and arrogant royal Atherton Wing."},
  { title: "Safe", description: "While visiting an outlying planet, Simon and River are kidnapped by local villagers in need of a doctor. Meanwhile, the rest of the crew are forced to seek medical help from an unlikely source when one of their own is shot."},
  { title: "Ariel", description: "Serenity travels to the core world Ariel and Simon comes up with a plan to steal drugs from the hospital."},
  { title: "War Stories", description: "Niska catches up with the Serenity crew."},
  { title: "Objects in Space", description: "The Alliance has placed a bounty on River Tam's head and a bounty Hunter named Jubal Early sets out to claim the bounty and tracks down the Serenity crew. Jubal sneaks on-board the Serenity while the crew is asleep to capture River and Simon and return them to the Alliance, so he can be paid for their capture and only River can stop him."},
  { title: "Serenity", description: "In the first half of the original pilot episode, the crew of Serenity takes on several passengers and make their way to another planet to try and sell goods they salvaged from a wrecked ship. Their efforts are impeded, though, when Wash and Mal realize that there is an undercover alliance fed on board. In the second half of the original pilot, Simon struggles to save Kaylee, and in turn River and himself while Mal, Zoe, and Jayne attempt to unload the cargo from the salvage to a trigger-happy mayor of a border moon." },
  { title: "Heart of Gold", description: "The Serenity responds to a distress call from Nandi, an old friend of Inara's, who is running a brothel on the moon of Deadwood. The unsavory leader of the moon's only town where the brothel is located has gotten Petaline, one the girls, pregnant and is demanding that he be given the baby. Nandi enlists the help of the crew of Serenity to defend the brothel and keep the baby with its mother." },
  { title: "Trash", description: "Saffron returns to offers the crew a mission but not everyone is willing to trust her again." },
  { title: "The Message", description: "A dying request from an old army buddy turns treacherous for Mal, Zoë, and the rest of the crew." }
]

user = User.where(name: "Joss", surname: "Whedon").first_or_create(name: "Joss", surname: "Whedon", email: "joss@whedon.org", password: "12345678", password_confirmation: "12345678")

MOVIES.each do |mov|
  user.movies.where(title: mov[:title]).first_or_create(mov)
end