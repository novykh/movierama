class CreateMovies < ActiveRecord::Migration[5.0]
  def change
    create_table :movies do |t|
      t.string :title, null: false, default: ""
      t.text :description, null: false
      t.references :user, foreign_key: true
      t.integer :likes_count, null: false, default: 0
      t.integer :hates_count, null: false, default: 0

      t.timestamps
    end
    add_index :movies, :created_at
    add_index :movies, :likes_count
    add_index :movies, :hates_count
  end
end
