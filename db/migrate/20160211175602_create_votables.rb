class CreateVotables < ActiveRecord::Migration[5.0]
  def change
    create_table :votables do |t|
      t.integer :user_id, null: false, foreign_key: true
      t.integer :movie_id, null: false, foreign_key: true
      t.string :vote_type, null: false, default: ""

      t.timestamps
    end
    add_index :votables, [:user_id, :movie_id], unique: true
    add_index :votables, :vote_type
  end
end
