class MoviePresenter

  def initialize(movie = nil, user = nil)
    @movie = movie
    @user = user
  end

  def is_votable?
    return false unless @user
    vote.present? ? false : true
  end

  def vote_type
    vote.try(:vote_type)
  end

  def vote
    @vote ||= @movie.votables.of_user(@user.try(:id)).first
  end

  def method_missing(method)
    @movie.send(method) rescue nil
  end
end