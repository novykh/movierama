class Voting
  include ActiveModel::Validations

  attr_reader :movie_id, :user_id, :vote_type, :notice

  validate :presence_of_user
  validate :presence_of_movie
  validate :user_must_not_own_movie
  validate :duplicate_voting

  def initialize(movie_id: nil, user_id: nil, vote_type: "")
    @movie_id = movie_id
    @user_id = user_id
    @vote_type = vote_type
  end

  def submit
    return self unless valid?
    return destroy unless vote_type.present?

    if votable = Votable.where(movie_id: movie_id, user_id: user_id).first
      votable.update_attribute(:vote_type, vote_type)
    else
      Votable.create(movie_id: movie_id, user_id: user_id, vote_type: vote_type)
    end

    @notice = "Vote has been submitted"
    self
  end

  private

  def movie
    @movie ||= Movie.find(movie_id) rescue nil
  end

  def user
    @user ||= User.find(user_id) rescue nil
  end

  def existing_vote
    @existing_vote ||= movie.votables.of_user(user.try(:id)).first
  end

  def presence_of_user
    errors.add(:base, 'User doesn\'t exist') unless user.present?
  end

  def presence_of_movie
    errors.add(:base, 'Movie doesn\'t exist') unless movie.present?
  end

  def user_must_not_own_movie
    return nil unless user.present? && movie.present?
    errors.add(:base, 'You cannot vote for your own movie') if movie.user == user
  end

  def duplicate_voting
    return nil unless user.present? && movie.present? && vote_type.present?
    errors.add(:base, "You have already #{vote_type}d this movie") if existing_vote.present? && existing_vote.vote_type == vote_type
  end

  def destroy
    return self unless valid?

    votable = Votable.where(movie_id: movie_id, user_id: user_id).first
    if votable
      votable.destroy!
      @notice = "Vote has been removed"
    else
      errors.add(:base, 'Couldn\'t find a vote to remove')
    end
    self
  end

end