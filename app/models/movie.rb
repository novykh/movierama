class Movie < ApplicationRecord

  SORT_COLUMNS = %w(likes_count hates_count created_at)

  belongs_to :user
  has_many :votables

  validates :title, :description, :user, presence: true

  after_touch :update_vote_counting

  scope :of_user, -> (user_id) { where(user_id: user_id) }

  def self.custom_sort_by(col)
    raise "Invalid column to sort by" unless SORT_COLUMNS.include?(col)
    Movie.all.order("#{col} desc")
  end

  def has_votes?
    likes_count > 0 || hates_count > 0
  end

  private

  def update_vote_counting
    self.update(likes_count: votables.likes.count, hates_count: votables.hates.count)
  end
end
