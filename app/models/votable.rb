class Votable < ApplicationRecord
  belongs_to :user
  belongs_to :movie, touch: true

  validates :user, :movie, :vote_type, presence: true
  validate :user_owns_movie
  validate :duplicate_for_user

  scope :likes, -> { where(vote_type: "like") }
  scope :hates, -> { where(vote_type: "hate") }
  scope :of_user, -> (user_id) { where(user_id: user_id) }

  private

  def user_owns_movie
    errors.add(:base, 'User cannot vote for his own movie') if movie && user == movie.user
  end

  def duplicate_for_user
    existing_vote = Votable.of_user(user).first
    errors.add(:base, "User cannot #{vote_type} again the same movie") if existing_vote.present? && existing_vote.vote_type == vote_type
  end
end
