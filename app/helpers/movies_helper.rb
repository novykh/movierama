module MoviesHelper

  def link_to_vote(string, movie, options = {})
    return "" unless movie.present?
    can_be_voted = user_signed_in? && options[:vote] != movie.vote_type && current_user.id != movie.user_id

    link_to_if can_be_voted, string, vote_movie_path(id: movie.id, vote: options[:vote]), class: options[:class], method: :post
  end

  def link_to_like(movie = nil, options = {})
    link_to_vote "Like (#{movie.likes_count})", movie, class: options[:class], vote: "like"
  end

  def link_to_hate(movie = nil, options = {})
    link_to_vote "Hate (#{movie.hates_count})", movie, class: options[:class], vote: "hate"
  end

  def link_to_delete_vote(movie = nil, options = {})
    if movie.vote_type.present?
      link = link_to_vote "Un#{movie.vote_type}", movie, class: options[:class], vote: nil
      content_tag(:span, "You #{movie.vote_type} this movie | #{link}".html_safe, class: "pull-right")
    end
  end

  def posted_period(movie)
    distance_of_time_in_words(movie.created_at, Time.now)
  end
end
