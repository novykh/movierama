class MoviesController < ApplicationController

  before_action :authenticate_user!, except: [:index]
  before_action :set_movie, only: [:vote]

  def index
    @params = params.to_unsafe_h.except(:controller, :action)

    @movies = Movie.includes(:user).all
    @movies = @movies.of_user(params[:user_id]) if params[:user_id]
    @movies = @movies.custom_sort_by(params[:sort_by]) if params[:sort_by]
    @movies = @movies.collect {|movie| MoviePresenter.new(movie, current_user) }
  end

  def new
    @movie = Movie.new
  end

  def create
    @movie = current_user.movies.new(movie_params)

    respond_to do |format|
      if @movie.save
        format.html { redirect_to movies_path, notice: 'Movie was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  def vote
    votable = Voting.new({ user_id: current_user.id, movie_id: @movie.id, vote_type: params[:vote] }).submit

    respond_to do |format|
      unless votable.errors[:base].present?
        format.html { redirect_to :back, notice: votable.notice }
      else
        format.html { redirect_to :back, alert: votable.errors[:base].join('\n') }
      end
    end
  end

  private

  def set_movie
    @movie = Movie.find(params[:id])
  end

  def movie_params
    params.require(:movie).permit(:title, :description)
  end
end
