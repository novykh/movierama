## README

* Ruby version

ruby-2.3.0

* Database creation & initialization

rails db:create db:migrate db:seed

* How to run the test suite

rake test
