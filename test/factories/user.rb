FactoryGirl.define do
  factory :user do
    name "John"
    surname  "Doe"
    sequence(:email) { |n| "#{n}jdoe@email.com" }
    password "12345678"
    password_confirmation "12345678"
  end
end