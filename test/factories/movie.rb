FactoryGirl.define do
  factory :movie do
    sequence(:title) { |n| "Title #{n}" }
    description "Description"

    after(:build) do |movie|
      movie.user = build(:user)
    end

    factory :movie_with_votes do
      likes_count 1
      hates_count 0

      after(:create) do |movie, evaluator|
        create_list(:votable, evaluator.likes_count, movie: movie, user: create(:user), vote_type: "like")
        create_list(:votable, evaluator.hates_count, movie: movie, user: create(:user), vote_type: "hate")
      end
    end
  end
end