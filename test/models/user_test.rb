require "test_helper"

class UserTest < ActiveSupport::TestCase

  let(:user) { build(:user) }

  it 'is valid' do
    user.must_be :valid?
  end

  it 'is invalid without email' do
    user.email = nil

    user.wont_be :valid?
    user.errors[:email].must_be :present?
  end

  it 'is invalid without bad email syntax' do
    user.email = "bad-email-syntax"

    user.wont_be :valid?
    user.errors[:email].must_be :present?
  end

  it 'is invalid without name' do
    user.name = nil

    user.wont_be :valid?
    user.errors[:name].must_be :present?
  end

  it 'is invalid without surname' do
    user.surname = nil

    user.wont_be :valid?
    user.errors[:surname].must_be :present?
  end

  it 'returns full name' do
    user.full_name.must_equal "#{user.name} #{user.surname}"
  end

end