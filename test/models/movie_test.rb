require "test_helper"

class MovieTest < ActiveSupport::TestCase

  let(:movie) { build(:movie) }
  let(:movie_with_votes) { build(:movie_with_votes) }

  it 'is valid' do
    movie.must_be :valid?
  end

  it 'is invalid without description' do
    movie.description = nil

    movie.wont_be :valid?
    movie.errors[:description].must_be :present?
  end

  it 'has not votes' do
    movie.has_votes?.must_equal false
  end

  it 'test has votes' do
    movie_with_votes.save

    movie_with_votes.has_votes?.must_equal true
  end

  context 'tests sorting' do
    it 'returns empty when no movies' do
      movies = Movie.custom_sort_by("likes_count")
      movies.must_equal []
    end

    it 'by likes count' do
      movie.save
      movie_with_votes.save

      movies = Movie.all
      (movies.first.likes_count < movies.last.likes_count).must_equal true

      movies = Movie.custom_sort_by("likes_count")
      (movies.first.likes_count < movies.last.likes_count).must_equal false
    end

    it 'returns error when invalid column' do
      err = assert_raises RuntimeError do
        Movie.custom_sort_by("title")
      end
      err.message.must_match "Invalid column to sort by"
    end
  end

end