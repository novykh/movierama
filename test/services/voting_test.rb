require "test_helper"

class MovieTest < ActiveSupport::TestCase

  let(:movie) { build(:movie) }
  let(:user) { build(:user) }
  let(:votable) { build(:votable, user: user, movie: movie) }

  context 'validations will fail when' do
    it 'has no user' do
      movie.save
      voting = Voting.new({movie_id: movie.id})

      voting.wont_be :valid?
      voting.errors[:base].must_be :present?
      voting.errors[:base].include?('User doesn\'t exist')
    end

    it 'has no movie' do
      user.save
      voting = Voting.new({user_id: user.id})

      voting.wont_be :valid?
      voting.errors[:base].must_be :present?
      voting.errors[:base].include?('Movie doesn\'t exist')
    end

    it 'user has already vote same movie with same type' do
      vote_type = "like"
      movie.save
      user.save
      votable.vote_type = vote_type
      votable.save
      voting = Voting.new({movie_id: movie.id, user_id: user.id, vote_type: vote_type})

      voting.wont_be :valid?
      voting.errors[:base].must_be :present?
      voting.errors[:base].include?("You have already #{vote_type}d this movie")
    end

    it 'user votes his own movie' do
      movie.save
      user = movie.user
      voting = Voting.new({movie_id: movie.id, user_id: user.id})

      voting.wont_be :valid?
      voting.errors[:base].must_be :present?
      voting.errors[:base].include?('You cannot vote for your own movie')
    end
  end

  context 'destroy existing votable' do
    it 'when vote type is not present' do
      movie.save
      user.save
      votable.vote_type = "like"
      votable.save
      Voting.new({movie_id: movie.id, user_id: user.id, vote_type: ""}).submit

      Votable.exists?(votable.id).must_equal false
    end

    it 'will be invalid when no existing found' do
      movie.save
      user.save
      voting = Voting.new({movie_id: movie.id, user_id: user.id, vote_type: ""}).submit

      voting.errors[:base].must_be :present?
      voting.errors[:base].include?('Movie doesn\'t exist')
    end
  end

  it 'will create votable when vote type is present' do
    vote_type = "like"
    movie.save
    user.save
    result = Voting.new({movie_id: movie.id, user_id: user.id, vote_type: vote_type}).submit

    Votable.where(movie_id: movie.id, user_id: user.id, vote_type: vote_type).exists?.must_equal true
    result.must_equal "Vote has been submitted"
  end
end